<?php

namespace App\Http\Controllers;

use App\Models\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class PostsController extends Controller
{
    public function index()
    {
        //get all posts, sorted by newest creation date
        $posts = Posts::orderBy('posts.created_at', 'desc')
            ->join('users', 'users.id', '=', 'posts.author_id')
            ->select('posts.id','posts.title','posts.abstract','posts.body','posts.created_at','posts.updated_at','users.name','users.user')
            ->where('posts.published', true)
            ->get();

        //return to view
        return view('posts.index', ['posts' => $posts]);

    }

    public function show($id)
    {
        //find one post by id
        $posts = Posts::join('users', 'users.id', '=', 'posts.author_id')
            ->select('posts.id','posts.title','posts.abstract','posts.body','posts.created_at','posts.updated_at','users.name','users.user')
            ->where('posts.published', true)
            ->find($id);

        //return to view
        return view('posts.single', ['posts'=> $posts]);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {

        // validation
        $this->validate($request, [
            'author_id' => 'required|numeric',
            'title'     => 'required',
            'slug'      => 'required',
            'abstract'  => 'required',
            'body'      => 'required'
         ]);

        $input = $request->all();

        DB::insert('insert into posts (author_id, title, slug, abstract, body, published, created_at, updated_at) values (?, ?, ?, ?, ?, ?, NOW(), NOW())',
                   [$input['author_id'],$input['title'],$input['slug'],$input['abstract'],$input['body'],$input['published'] == 'on'? true : false]);
        // redirect
        return redirect('/posts');

    }

    public function update($id, Request $request)
    {
        // Upon update, update the timestamp
        $post = Posts::where('id', $id)
            ->update(['updated_at' => NOW()]);

        return redirect('/posts');
    }

    public function destroy($id)
    {
        //find one post by id then delete
        $post = DB::table('posts')->where('id', $id);
        $post->delete();

        return redirect('/posts');
    }
}

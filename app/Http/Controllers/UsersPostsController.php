<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Posts;
use Illuminate\Http\Request;

class UsersPostsController extends Controller
{

    public function index()
    {
        abort(404);
    }

    public function show(Request $request)
    {

        $path = explode('/',$request->path());

        if( count($path) == 2 )
        {
            //find all posts by user
            $posts = Posts::orderBy('posts.created_at', 'desc')
                ->join('users', 'users.id', '=', 'posts.author_id')
                ->select('posts.id','posts.title','posts.abstract','posts.body','posts.created_at','posts.updated_at','users.name','users.user')
                ->where('posts.published', true)
                ->where('users.user', $path[1])
                ->get();

            //return to view
            return view('posts.index', ['posts'=> $posts]);
        } 
        elseif(count($path) == 4)
        {
            //find one post by user and id
            $posts = Posts::join('users', 'users.id', '=', 'posts.author_id')
                ->select('posts.id','posts.title','posts.abstract','posts.body','posts.created_at','posts.updated_at','users.name','users.user')
                ->where('posts.published', true)
                ->where('users.user', $path[1])
                ->find($path[3]);

            if(is_null($posts))
            {
                abort(404);
            }

            return view('posts.single', ['posts'=> $posts]);
        }
        else
        {
            abort(404);
        }
    }
}

<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PostsController;
use App\Http\Controllers\UsersPostsController;
use App\Http\Controllers\CategoriesPostsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('posts', PostsController::class);
Route::resource('users', UsersPostsController::class);
Route::resource('users.posts', UsersPostsController::class);
Route::resource('categories.posts', CategoriesPostsController::class);


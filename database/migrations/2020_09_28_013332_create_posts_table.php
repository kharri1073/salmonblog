<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;



class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('author_id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('abstract');
            $table->text('body');
            $table->boolean('published')->default(false);
            $table->timestamps();
        });
        
        DB::table('posts')->insert(
            array([
                'author_id' => 1,
                'title' => 'first title',
                'slug' => 'first-slug',
                'abstract' => 'the abstract',
                'body' => 'the body',
                'published' => true,
                'created_at' => date('c'),
                'updated_at' => date('c')
            ],[
                'author_id' => 1,
                'title' => 'second title',
                'slug' => 'second-slug',
                'abstract' => 'the abstract',
                'body' => 'the body22',
                'published' => true,
                'created_at' => date('c'),
                'updated_at' => date('c')
            ],[
                'author_id' => 1,
                'title' => 'third title',
                'slug' => 'third-slug',
                'abstract' => 'the abstract',
                'body' => 'the body33',
                'published' => true,
                'created_at' => date('c'),
                'updated_at' => date('c')
            ],[
                'author_id' => 2,
                'title' => 'Bob title 1',
                'slug' => 'bob-slug',
                'abstract' => 'the abstract',
                'body' => 'the body for bob',
                'published' => true,
                'created_at' => date('c'),
                'updated_at' => date('c')
            ],[
                'author_id' => 2,
                'title' => 'bob 2',
                'slug' => 'bob-slug-2',
                'abstract' => 'the abstract 22',
                'body' => 'the body 3334442211',
                'published' => true,
                'created_at' => date('c'),
                'updated_at' => date('c')
            ]
            )
        );
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

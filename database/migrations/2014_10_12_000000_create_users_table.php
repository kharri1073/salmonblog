<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
	    $table->string('name');
	    $table->string('user');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert(
            array([
                'id' => 1,
                'name' => "kevin harrison",
                'user' => "kevin",
                'email' => "kharri1073@gmail.com",
                'password' => "secretpass",
                'remember_token' => "remember yet?",
                'created_at' => date('c'),
                'updated_at' => date('c')
            ],[
                'id' => 2,
                'name' => 'bobby mcbobface',
                'user' => 'bob',
                'email' => 'thebobber@gmail.com',
                'password' => 'hunter2',
                'remember_token' => '*******',
                'created_at' => date('c'),
                'updated_at' => date('c')
            ]
            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

@extends('main')

@section('content')

    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

            <h1 class="mt-4"><a href="{{ route('posts.show',$posts->id) }}">{{$posts->title}}</a></h1>

            <!-- Author -->
            <p class="lead">
                by
                <a href="/users/{{$posts->user}}">{{$posts->name}}</a>
            </p>

            <hr>

            <!-- Date/Time -->
            <p>Posted on {{$posts->created_at}} and updated at {{$posts->updated_at}}</p>

            <hr>

            <!-- Post Content -->
            {!! $posts->body !!}
            <hr>

            <!-- Specials -->
            <form action="{{ route('posts.update', $posts->id) }}" method="POST">
                @csrf
                @method('PUT')
                <input type="hidden" name="updated_at" value="now()" />
                <button type="submit" class="btn btn-danger">Update</button>
            </form>
            <form action="{{ route('posts.destroy', $posts->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>

        </div>

    </div>

@endsection

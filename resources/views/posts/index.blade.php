@extends('main')

@section('content')

    @foreach ($posts as $post)
    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

            <h1 class="mt-4"><a href="{{ route('posts.show',$post->id) }}">{{$post->title}}</a></h1>

            <!-- Author -->
            <p class="lead">
                by
                <a href="/users/{{ $post->user }}">{{$post->name}}</a>
            </p>

            <hr>

            <!-- Date/Time -->
            <p>Posted on {{$post->created_at}}</p>

            <hr>

            <!-- Post Content -->
            {!! $post->body !!}
            <hr>

            <!-- Special -->
            <a href="{{ route('posts.create')}}">Create New Post</a>


        </div>

    </div>
    @endforeach

@endsection

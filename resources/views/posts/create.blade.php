@extends('main')

@section('content')

    <form action="{{ route('posts.store') }}" method="POST">
        @csrf
        <label for="author_id">Author ID</label>
        <input id="author_id" name="author_id" type="text" class="@error('author_id') is-invalid @enderror"><br>

        <label for="title">Title</label>
        <input id="title" name="title" type="text" class="@error('title') is-invalid @enderror"><br>

        <label for="slug">Slug</label>
        <input id="slug" name="slug" type="text" class="@error('slug') is-invalid @enderror"><br>

        <label for="abstract">Abstract</label>
        <input id="abstract" name="abstract" type="text" class="@error('abstract') is-invalid @enderror"><br>

        <label for="body">Body</label>
        <textarea id="body" name="body" class="@error('body') is-invalid @enderror" cols="60" rows="10"></textarea><br>

        <label for="published">Published</label>
        <input id="published" name="published" type="checkbox" checked class="@error('published') is-invalid @enderror"><br>

        <button type="submit" class="btn btn-primary">Create</button>
    </form>

@endsection
